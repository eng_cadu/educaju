import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DadosInstituicaoComponent } from './gestor/dados-instituicao/dados-instituicao.component';
import { GestorComponent } from './gestor/gestor.component';
import { ListaCursoComponent } from './gestor/curso/lista-curso/lista-curso.component';
import { EditCursoComponent } from './gestor/curso/edit-curso/edit-curso.component';
import { AlunoComponent } from './gestor/aluno/aluno.component';
import { ListaAlunoComponent } from './gestor/aluno/lista-aluno/lista-aluno.component';
import { SalaAulaComponent } from './sala-aula/sala-aula.component';



const routes: Routes = [
	{ path: 'gestor/dados-instituicao', component: DadosInstituicaoComponent, pathMatch: 'full'},
	{ path: 'sala', component: SalaAulaComponent, pathMatch: 'full'},
	{ path: 'cursos/edit', component: EditCursoComponent, pathMatch: 'full'},
	{ path: 'cursos', component: ListaCursoComponent, pathMatch: 'full'},
	{ path: 'alunos', component: ListaAlunoComponent, pathMatch: 'full'},
	{ path: 'gestor', component: GestorComponent, pathMatch: 'full'},

];

@NgModule({
	imports: [
		RouterModule.forRoot(routes),
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }