import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DiscenteComponent } from './discente/discente.component';
import { CursoComponent } from './gestor/curso/curso.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AppRoutingModule } from './app-routing.module';
import { LeftBarComponent } from './left-bar/left-bar.component';
import { DadosInstituicaoComponent } from './gestor/dados-instituicao/dados-instituicao.component';
import { ListaCursoComponent } from './gestor/curso/lista-curso/lista-curso.component';
import { GestorComponent } from './gestor/gestor.component';
import { EditCursoComponent } from './gestor/curso/edit-curso/edit-curso.component';
import { AlunoComponent } from './gestor/aluno/aluno.component';
import { ListaAlunoComponent } from './gestor/aluno/lista-aluno/lista-aluno.component';
import { SalaAulaComponent } from './sala-aula/sala-aula.component';

@NgModule({
  declarations: [
    AppComponent,
    DiscenteComponent,
    CursoComponent,
    TopBarComponent,
    LeftBarComponent,
    DadosInstituicaoComponent,
    ListaCursoComponent,
    GestorComponent,
    EditCursoComponent,
    AlunoComponent,
    ListaAlunoComponent,
    SalaAulaComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
