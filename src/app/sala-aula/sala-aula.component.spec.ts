import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalaAulaComponent } from './sala-aula.component';

describe('SalaAulaComponent', () => {
  let component: SalaAulaComponent;
  let fixture: ComponentFixture<SalaAulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaAulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalaAulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
